Jacob Sorber's better hash table
================================


This is the code generated from YouTube content:

- https://youtu.be/KI_V91UdL1I
- https://youtu.be/OQHDEhLKVv0
- https://youtu.be/MXdF-n-hAQU

Hashing functions:
------------------

```sh
$ ./test words.txt 500000  # hash
Loaded 600 words into the table.
	...with 156 collisions
554 out of 500000 guesses were in the table
$ ./test words.txt 500000  # hash_fnv1
Loaded 600 words into the table.
	...with 137 collisions
554 out of 500000 guesses were in the table
$ ./test words.txt 500000  # hash_fnv1a
Loaded 600 words into the table.
	...with 126 collisions
554 out of 500000 guesses were in the table
```
